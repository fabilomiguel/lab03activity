public class Application{
	
	public static void main(String[] args)
	{
		Student[] section3 = new Student[3];
		
		Student A = new Student();
		Student B = new Student(); 
		
		section3[0] = A;
		section3[1] = B;
		section3[2] = new Student();
		section3[2].semester = 5;
		section3[2].name = "Peter";
		section3[2].gender = 'M';
			
		System.out.println(section3[2].name +" " + section3[2].semester + " " + section3[2].gender);
		
		
		A.semester = 1;
		B.semester = 2;
		A.name = "John";
		B.name = "Mary";
		A.gender = 'M';
		B.gender = 'F';
		
		//System.out.println("The first student is: "+ A.name +", studing the semester number: "+A.semester +", gender: "+ A.gender+".");
		//System.out.println("The second student is: "+ B.name +", studing the semester number: "+B.semester +", gender: "+ B.gender+".");
	
		//System.out.println(Student.Study());
		A.Study();
		B.SkippingClass();
	}
}